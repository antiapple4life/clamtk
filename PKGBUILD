# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>
# Contributor: Blaž Tomažič <blaz.tomazic@gmail.com>

pkgname=clamtk
pkgver=6.18
pkgrel=1
pkgdesc='Easy to use, light-weight, on-demand virus scanner for Linux systems'
url='https://gitlab.com/dave_m/clamtk/-/wikis/home'
arch=('any')
license=('GPL')
depends=('clamav' 'perl' 'perl-gtk3' 'perl-locale-gettext' 'perl-libwww' 'perl-http-message'
         'perl-lwp-protocol-https' 'perl-text-csv' 'perl-json' 'python' 'zenity' 'desktop-file-utils'
         'cron')
source=(https://github.com/dave-theunsub/clamtk/releases/download/v${pkgver}/clamtk-${pkgver}.tar.xz{,.asc})
# Hashes sourced from https://gitlab.com/dave_m/clamtk/-/wikis/ClamTk-Hashes
sha256sums=('02de0e29d6edb18efb6f7400c4230d8ce006d76cd30c5f01fec58a14d5d85afe'
            '34e5da1a723684b9533d4dce6610c5673d9285fc30dab643868a676268c43ad8')
md5sums=('5f9e90b5d8d2d5f0fb88fe9c7882e511'
         'a1e5785095af0fb8fdd7e32996d64eda')
validpgpkeys=('DC18B5EB12B82897DF0D0AAEC81DF0FAC4AFEB22') # Dave Mauroni <dave.nerd@gmail.com>

package() {
  cd ${pkgname}-${pkgver}

  install -Dm 755 clamtk -t "${pkgdir}/usr/bin"
  for f in lib/* ; do
    install -Dm 644 "${f}" "${pkgdir}/usr/share/perl5/vendor_perl/ClamTk/$(basename "${f}")"
  done

  for f in po/*.mo ; do
    install -Dm 644 "${f}" "${pkgdir}/usr/share/locale/$(basename "${f}" .mo)/LC_MESSAGES/clamtk.mo"
  done

  install -Dm 644 clamtk.1.gz -t "${pkgdir}/usr/share/man/man1"
  install -Dm 644 CHANGES DISCLAIMER.md README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"

  install -Dm 644 images/clamtk.{xpm,png} -t "${pkgdir}/usr/share/pixmaps"
  install -Dm 644 clamtk.desktop -t "${pkgdir}/usr/share/applications"
}

# vim: ts=2 sw=2 et:
